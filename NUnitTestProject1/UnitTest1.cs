using NUnit.Framework;
using System.Net.Http;
using CryptoTracker;
using System.Linq;
using System.Collections.Generic;

namespace Tests
{
    public class Tests
    {

        APILoader apiloader;
        DataStorage datastorage;

        [SetUp]
        public void Setup()
        {
            apiloader = new APILoader();
            datastorage = new DataStorage();

        }

        [Test]
        public async void ApiResponseConnection()
        {
            
            await apiloader.GetCurrencies();


            Assert.AreNotEqual(apiloader.cmcResponse, null);

        }

        [Test]
        public async void CoinDetailsTest()
        {

            await apiloader.GetCurrencies();


            Assert.AreEqual(apiloader.cmcResponse.Data[0].CmcRank, 1);
            Assert.AreEqual(apiloader.cmcResponse.Data[0].Name.ToLower(), "bitcoin");
            Assert.AreEqual(apiloader.cmcResponse.Data[0].Slug, "BTC");


        }

        [Test]
        public async void FavoritesAddCoinTest()
        {
            await apiloader.GetCurrencies();

            List<Datum> datumlist = apiloader.cmcResponse.Data;
            datastorage.AddToFavorites(datumlist[0]);
            List<Datum> favoritelist = datastorage.GetFavorites();
            Assert.AreEqual(favoritelist[0].Name, "Bitcoin");
        }

        [Test]
        public void FavoritesRemoveCoinTest()
        {
            List<Datum> datumlist = apiloader.cmcResponse.Data;
            datastorage.AddToFavorites(datumlist[0]);
            datastorage.AddToFavorites(datumlist[1]);
            datastorage.RemoveFavorite(datumlist[0]);
            List<Datum> favoritelist = datastorage.GetFavorites();
            Assert.AreNotEqual(favoritelist[0], "Ethereum");
        }

        [Test]
        public void InvestmentsAddCoinTest()
        {
            Assert.Pass();
        }

        [Test]
        public void InvestmentsRemoveCoinTest()
        {
            Assert.Pass();
        }

        [Test]
        public async void SearchBarTest()
        {

           string keyword = "0";

            await apiloader.GetCurrencies();

            var matchingResults = apiloader.cmcResponse.Data.Where(c => c.Name.ToLower().Contains(keyword.ToLower())).ToList();

            Assert.AreEqual(matchingResults[0].Name, "0x");
        }

        [Test]
        public async void SortByRankTest()
        {
            await apiloader.GetCurrencies();

            var matchingResults = apiloader.cmcResponse.Data.OrderBy(c => c.CmcRank).ToList();

            Assert.AreEqual(matchingResults[0].CmcRank, 1);
        }

        [Test]
        public async void SortByNameTest()
        {
            await apiloader.GetCurrencies();

            var matchingResults = apiloader.cmcResponse.Data.OrderBy(c => c.Name).ToList();

            Assert.AreEqual(matchingResults[0].Name, "0x");
        }

        [Test]
        public async void SortByMarketCapTest()
        {
            await apiloader.GetCurrencies();

            var matchingResults = apiloader.cmcResponse.Data.OrderBy(c => c.Quote.Usd.MarketCap).ToList();

            Assert.AreNotEqual(matchingResults[0].Quote.Usd.MarketCap, null);
        }

        [Test]
        public async void SortByPriceTest()
        {
            await apiloader.GetCurrencies();

            var matchingResults = apiloader.cmcResponse.Data.OrderBy(c => c.Quote.Usd.Price).ToList();

            Assert.AreNotEqual(matchingResults[0], null);
        }


        [Test]
        public async void SortByVolume24hTest()
        {
            await apiloader.GetCurrencies();

            var matchingResults = apiloader.cmcResponse.Data.OrderBy(c => c.Quote.Usd.Volume24H).ToList();

            Assert.AreNotEqual(matchingResults[0].Quote.Usd.Volume24H, null);
        }

        [Test]
        public async void SortByCirculatingSupplyTest()
        {
            await apiloader.GetCurrencies();

            var matchingResults = apiloader.cmcResponse.Data.OrderBy(c => c.CirculatingSupply).ToList();

            Assert.AreEqual(matchingResults[0].CirculatingSupply, null);
        }

        [Test]
        public async void SortByChange24hTest()
        {
            await apiloader.GetCurrencies();

            var matchingResults = apiloader.cmcResponse.Data.OrderBy(c => c.Quote.Usd.PercentChange24H).ToList();

            Assert.AreNotEqual(matchingResults[0].Quote.Usd.PercentChange24H, null);
        }

        
    }

}
