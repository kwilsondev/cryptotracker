﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace CryptoTracker
{
    public partial class MasterPage : ContentPage
    {
        
        MasterDetailPage mdp;

        public MasterPage()
        {
            InitializeComponent();

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            mdp = Application.Current.MainPage as MasterDetailPage;
        }

        void OverviewTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Overview");

            mdp.IsPresented = false;
           
            OverviewPage newPage = new OverviewPage();

            if (Device.RuntimePlatform != Device.iOS)
            {
                newPage.Title = "Overview";
            }

            NavigationPage pageWrapper = new NavigationPage(newPage);
            pageWrapper.BarTextColor = Color.White;
            pageWrapper.BarBackgroundColor = Color.FromHex("#9b1006");

            mdp.Master.Title = "Overview";

            mdp.Detail = pageWrapper;
        }

        void FavoritesTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Favorites");

            mdp.IsPresented = false;

            FavoritesPage newPage = new FavoritesPage();

            if (Device.RuntimePlatform != Device.iOS)
            {
                newPage.Title = "Favorites";
            }

            NavigationPage pageWrapper = new NavigationPage(newPage);
            pageWrapper.BarTextColor = Color.White;
            pageWrapper.BarBackgroundColor = Color.FromHex("#9b1006");

            mdp.Master.Title = "Favorites";

            mdp.Detail = pageWrapper;
        }

        void InvestmentsTapped(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Investments");

            mdp.IsPresented = false;

            InvestmentsPage newPage = new InvestmentsPage();

            if (Device.RuntimePlatform != Device.iOS)
            {
                newPage.Title = "Investments";
            }

            NavigationPage pageWrapper = new NavigationPage(newPage);
            pageWrapper.BarTextColor = Color.White;
            pageWrapper.BarBackgroundColor = Color.FromHex("#9b1006");

            mdp.Master.Title = "Investments";

            mdp.Detail = pageWrapper;
        }

    }
}
