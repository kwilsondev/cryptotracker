﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Xamarin.Forms;

namespace CryptoTracker
{
    public partial class CurrencyDetailPage : ContentPage
    {
        public String currencyName, currencySym;
        public Decimal currencyPrice;
        public Datum Currency;
        
        public CurrencyDetailPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            checkInvested();
        }

        public CurrencyDetailPage(Datum currency)
        {
            this.currencyPrice = (Decimal)currency.Quote.Usd.Price;
            this.currencyName = currency.Name;
            this.Currency = currency;
            InitializeComponent();
            lblName.Text += currencyName;
            lblTicker.Text += currency.Symbol;
            currencySym = currency.Symbol;
            btnAddInvestment.Text += currency.Symbol;
            lblPrice.Text += currency.Quote.Usd.Price;
            lblSupply.Text += currency.TotalSupply;
            lblVolume.Text += currency.Quote.Usd.Volume24H;

            lbl12hr.Text += "";
            // if(change > 0) lbl12hr.TextColor = Xamarin.Forms.Color.Green;
            // else if (change < 0) lbl12hr.TextColor = Xamarin.Forms.Color.Red;

            lbl24hr.Text += currency.Quote.Usd.PercentChange24H;
            if(currency.Quote.Usd.PercentChange24H > 0) lbl24hr.TextColor = Xamarin.Forms.Color.Green;
            else if (currency.Quote.Usd.PercentChange24H < 0) lbl24hr.TextColor = Xamarin.Forms.Color.Red;

            lbl3Day.Text += "";
            // if(change > 0) lbl3Day.TextColor = Xamarin.Forms.Color.Green;
            //else if (change < 0) lbl3Day.TextColor = Xamarin.Forms.Color.Red;

            lbl7Day.Text += currency.Quote.Usd.PercentChange7D;
            if(currency.Quote.Usd.PercentChange7D > 0) lbl7Day.TextColor = Xamarin.Forms.Color.Green;
            else if (currency.Quote.Usd.PercentChange7D < 0) lbl7Day.TextColor = Xamarin.Forms.Color.Red;

            lbl1Month.Text += "";
            // if(change > 0) lbl1Month.TextColor = Xamarin.Forms.Color.Green;
            //else if (change < 0) lbl1Month.TextColor = Xamarin.Forms.Color.Red;

            string currentCoin = lblName.Text.Split(':')[1].Trim();

            List<Datum> Favorites = App.DataStorage.GetFavorites();
            if (!Favorites.Any(c => c.Name.ToLower() == Currency.Name.ToLower()))
            {
                btnAddFavorite.Text = "Add to Favorites";
            }
            else
            {
                btnAddFavorite.Text = "Remove From Favorites";
            }

            checkInvested();
            
        }

        private void btnAddFavorite_Clicked(object sender, EventArgs e)
        {
            /*if (favorited){
             *  unfavorite();
             *  btnAddFavorite.Text = "Add to Favorites";
             * }
             * else {
             *  favorite();
             *  btnAddFavorite.Text = "Remove from Favorites";
             * }
             */

            List<Datum> Favorites = App.DataStorage.GetFavorites();


            if (Favorites.Any(c => c.Name.ToLower() == Currency.Name.ToLower()))
            {
                App.DataStorage.RemoveFavorite(Currency);
                btnAddFavorite.Text = "Add to Favorites";
            }
            else
            {
                App.DataStorage.AddToFavorites(Currency);
                btnAddFavorite.Text = "Remove From Favorites";
            }

        }

        private void BtnMoreDetails_Clicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://en.wikipedia.org/wiki/" + currencyName.Replace(' ','_')));
        }

        private void BtnAddInvestment_Clicked(object sender, EventArgs e)
        {
            
            int cont = 0;
            foreach (InvestmentInfo investment in App.DataStorage.GetInvestments())
            {
                if (investment.Name == currencyName)
                {
                    //query to change investment
                    cont = 1;

                    InvestmentEditPage editPage = new InvestmentEditPage(currencyName, currencyPrice, 1);
                    editPage.amtInvested = investment.AmtInvested;
                    editPage.oldPrice = investment.Price;
                    this.Navigation.PushAsync(editPage);
                    //App.DataStorage.RemoveInvestments(currencyName);
                    break;
                }
            }

            if (cont == 0)
            {
                //query for amount to invest in
                InvestmentInfo newBoi = new InvestmentInfo();
                newBoi.Name = currencyName;
                newBoi.Price = currencyPrice;
                newBoi.AmtInvested = decimal.One;
                InvestmentEditPage editPage = new InvestmentEditPage(currencyName, currencyPrice, 0);
                this.Navigation.PushAsync(editPage);
                //App.DataStorage.AddToInvestments(newBoi);
            }

            
        }

        void checkInvested()
        {
            List<InvestmentInfo> Investments = App.DataStorage.GetInvestments();
            if (!Investments.Any(c => c.Name.ToLower() == Currency.Name.ToLower()))
            {
                btnAddInvestment.Text = "Invest in " + currencySym;
            }
            else
            {
                btnAddInvestment.Text = "Sell " + currencySym;
            }
        }
    }
}
