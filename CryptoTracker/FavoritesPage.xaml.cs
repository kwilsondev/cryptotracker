﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace CryptoTracker
{
    public partial class FavoritesPage : ContentPage
    {

        List<Datum> Favorites;

        public FavoritesPage()
        {
            InitializeComponent();

            Favorites = App.DataStorage.GetFavorites();
            PopulateFavoritesPage(Favorites);

            Debug.WriteLine("");
        }

        public void FavoritesSearchBar_OnTextChanged(object sender, EventArgs e)
        {

        }

        public void PopulateFavoritesPage(List<Datum> currencies)
        {

            CurrenciesStack.Children.Clear();

            foreach (Datum c in currencies)
            {
                //NomicsApi twelveHrData = twelveHour.FirstOrDefault(n => n.Currency.ToLower() == c.Slug.ToLower());
                //NomicsApi threeDayData = threeDay.FirstOrDefault(n => n.Currency.ToLower() == c.Slug.ToLower());
                //NomicsApi oneMonthData = oneMonth.FirstOrDefault(n => n.Currency.ToLower() == c.Slug.ToLower());

                Frame f = new Frame
                {
                    BackgroundColor = Color.White,
                    BorderColor = Color.White,
                    CornerRadius = 10,
                    Padding = 10,
                    Margin = new Thickness(10, 5)

                };

                StackLayout vStack = new StackLayout
                {
                    Orientation = StackOrientation.Vertical
                };

                StackLayout line1 = new StackLayout
                {
                    Orientation = StackOrientation.Horizontal
                };

                StackLayout line2 = new StackLayout
                {
                    Orientation = StackOrientation.Horizontal
                };

                line1.Children.Add(new Label
                {
                    Text = c.Symbol + " - ",
                    BindingContext = c.Symbol + " - ",
                    FontSize = 15,
                    TextColor = Color.Black,
                    FontAttributes = FontAttributes.Bold
                });

                line1.Children.Add(new Label
                {
                    Text = c.Name,
                    BindingContext = c.Name,
                    FontSize = 15,
                    TextColor = Color.Black,
                    FontAttributes = FontAttributes.Bold
                });

                line1.Children.Add(new Label
                {
                    Text = "Price: " + c.Quote.Usd.Price.ToString("C4"),
                    BindingContext = c.Quote.Usd.Price.ToString("C4"),
                    FontSize = 15,
                    TextColor = Color.Black,
                    HorizontalOptions = LayoutOptions.EndAndExpand
                });

                line2.Children.Add(new Label
                {
                    Text = "24h Change: " + c.Quote.Usd.PercentChange24H.ToString("0.##") + "%",
                    BindingContext = c.Quote.Usd.PercentChange24H,
                    FontSize = 15,
                    TextColor = Color.Black,
                    HorizontalOptions = LayoutOptions.StartAndExpand
                });

                line2.Children.Add(new Label
                {
                    Text = "7d Change: " + c.Quote.Usd.PercentChange7D.ToString("0.##") + "%",
                    BindingContext = c.Quote.Usd.PercentChange7D,
                    FontSize = 15,
                    TextColor = Color.Black,
                    HorizontalOptions = LayoutOptions.EndAndExpand
                });

                vStack.Children.Add(line1);
                vStack.Children.Add(new BoxView { Color = Color.Black, HeightRequest = 2 });
                vStack.Children.Add(new Label
                {
                    Text = "Trade Volume: " + c.Quote.Usd.Volume24H.ToString("C2"),
                    BindingContext = c.Quote.Usd.Volume24H,
                    FontSize = 15,
                    TextColor = Color.Black,
                    HorizontalOptions = LayoutOptions.StartAndExpand
                });
                vStack.Children.Add(new Label
                {
                    Text = "Supply: " + c.TotalSupply.ToString("C2"),
                    BindingContext = c.TotalSupply,
                    FontSize = 15,
                    TextColor = Color.Black,
                    HorizontalOptions = LayoutOptions.StartAndExpand
                });
                vStack.Children.Add(line2);


                f.Content = vStack;

#pragma warning disable CS0618 // Type or member is obsolete
                f.GestureRecognizers.Add(new TapGestureRecognizer((obj) => {
                    CurrencyDetailPage currencyDetailPage = new CurrencyDetailPage(c);//stack.Children[0].BindingContext);
                    this.Navigation.PushAsync(currencyDetailPage);
                }));
#pragma warning restore CS0618 // Type or member is obsolete

                Device.BeginInvokeOnMainThread(() => {
                    CurrenciesStack.Children.Add(f);
                });

            }

        }
    }
}
