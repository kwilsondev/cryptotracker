﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CryptoTracker
{
    public class APILoader
    { 
        public HttpClient ApiClient { get; set; }
        public string cmcApiResponse;
        public string nomics12hr;
        public string nomics3day;
        public string nomicsMonth;
        public CoinMarketCapApi cmcResponse { get; set; }
        public List<NomicsApi> twelveHourNomicsResponse { get; set; }
        public List<NomicsApi> threeDayNomicsResponse { get; set; }
        public List<NomicsApi> oneMonthNomicsResponse { get; set; }

        public APILoader()
        {

            //Task.Run(async () => await LoadCurrencies());
         
        }

        public void InitializeClient()
        {
            ApiClient = new HttpClient();
            ApiClient.DefaultRequestHeaders.Accept.Clear();
            ApiClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            /*Task.Run(async () =>
            {
                await LoadCurrencies();

                return cmcResponse = CoinMarketCapApi.FromJson(apiResponse);

            });*/
        }   
        public async Task GetCurrencies()
        {
            //create the httpClient
            HttpClient client = new HttpClient();

            //holds our api key for Coin Market Cap
            string cmcApiKey = "fea5c05e-792e-4675-8acd-6ca3f711337b";

            //holds our api key for Coin Market Cap
            string nomicsApiKey = "3388aab18baeb92f6276de3097b474c6";

            //Get latest data for all coins from the coin market cap api
            var response = await client.GetStringAsync("https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?CMC_PRO_API_KEY=" + cmcApiKey);
            cmcApiResponse = response;
            cmcResponse = CoinMarketCapApi.FromJson(cmcApiResponse);

            string dateNow = GenerateDateForNomics(DateTime.UtcNow);
            string date12hr = GenerateDateForNomics(DateTime.UtcNow.Subtract(TimeSpan.FromHours(12)));
            string date3day = GenerateDateForNomics(DateTime.UtcNow.Subtract(TimeSpan.FromDays(3)));
            string dateMonth = GenerateDateForNomics(DateTime.UtcNow.Subtract(TimeSpan.FromDays(30)));


            ////Get the 12 hour nomics data
            //response = await client.GetStringAsync("https://api.nomics.com/v1/currencies/interval?key=" + 
            //                                            nomicsApiKey +
            //                                            "&start=" +
            //                                            date12hr +
            //                                            "&end=" +
            //                                            dateNow );
            //nomics12hr = response;
            //twelveHourNomicsResponse = NomicsApi.FromJson(nomics12hr);

            ////Get the 3 day nomics data
            //response = await client.GetStringAsync("https://api.nomics.com/v1/currencies/interval?key=" +
            //                                            nomicsApiKey +
            //                                            "&start=" +
            //                                            date3day +
            //                                            "&end=" +
            //                                            dateNow);
            //nomics3day = response;
            //twelveHourNomicsResponse = NomicsApi.FromJson(nomics3day);

            ////Get the 1 month nomics data
            //response = await client.GetStringAsync("https://api.nomics.com/v1/currencies/interval?key=" +
            //                                            nomicsApiKey +
            //                                            "&start=" +
            //                                            dateMonth +
            //                                            "&end=" +
            //                                            dateNow);
            //nomicsMonth = response;
            //twelveHourNomicsResponse = NomicsApi.FromJson(nomicsMonth);

            Debug.WriteLine("");
        }

        public string GenerateDateForNomics(DateTime date)
        {
            //build now date string for nomics
            StringBuilder NomicsDate = new StringBuilder();
            NomicsDate.Append(date.Year + "-");

            if(date.Month < 10)
            {
                NomicsDate.Append("0" + date.Month + "-");
            }
            else
            {
                NomicsDate.Append(date.Month + "-");
            }

            if (date.Day < 10)
            {
                NomicsDate.Append("0" + date.Day + "-");
            }
            else
            {
                NomicsDate.Append(date.Day + "T");
            }



            if (date.Hour < 10)
            {
                NomicsDate.Append("0" + date.Hour + "%3A");
            }
            else
            {
                NomicsDate.Append(date.Hour + "%3A");
            }

            if (date.Minute < 10)
            {
                NomicsDate.Append("0" + date.Minute + "%3A");
            }
            else
            {
                NomicsDate.Append(date.Minute + "%3A");
            }

            if (date.Second < 10)
            {
                NomicsDate.Append("0" + date.Second + "Z");
            }
            else
            {
                NomicsDate.Append(date.Second + "Z");
            }


            return NomicsDate.ToString();

        }

    }
}
