﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CryptoTracker
{
    public partial class OverviewPage : ContentPage
    {

        string apiResponse;
        CoinMarketCapApi cmcResponse;
        //List<NomicsApi> twelveHourNomics;
        //List<NomicsApi> threeDayNomics;
        //List<NomicsApi> oneMonthNomics;
        List<Datum> currentState;

        public OverviewPage()
        { 
        
            InitializeComponent();

           Task.Run(async () =>
           { 
                await App.APILoader.GetCurrencies();
                cmcResponse = App.APILoader.cmcResponse;
                //twelveHourNomics = App.APILoader.twelveHourNomicsResponse;
                //threeDayNomics = App.APILoader.threeDayNomicsResponse;
                //oneMonthNomics = App.APILoader.oneMonthNomicsResponse;

               PopulateOverviewPage(cmcResponse.Data);
           });

            SortPicker.Items.Add("Rank");
            SortPicker.Items.Add("Alphabetical");
            SortPicker.Items.Add("Market Cap");
            SortPicker.Items.Add("Price");
            SortPicker.Items.Add("Volume(24h)");
            SortPicker.Items.Add("Circulating Supply");
            SortPicker.Items.Add("Change(24h)");

            SortOrderPicker.Items.Add("ASC");
            SortOrderPicker.Items.Add("DSC");


            Debug.WriteLine("");

        }

        public void PopulateOverviewPage(List<Datum> currencies)
        {

            CurrenciesStack.Children.Clear();

            foreach (Datum c in currencies)
            {
                //NomicsApi twelveHrData = twelveHour.FirstOrDefault(n => n.Currency.ToLower() == c.Slug.ToLower());
                //NomicsApi threeDayData = threeDay.FirstOrDefault(n => n.Currency.ToLower() == c.Slug.ToLower());
                //NomicsApi oneMonthData = oneMonth.FirstOrDefault(n => n.Currency.ToLower() == c.Slug.ToLower());

                Frame f = new Frame
                {
                    BackgroundColor = Color.White,
                    BorderColor = Color.White,
                    CornerRadius = 10,
                    Padding = 10,
                    Margin = new Thickness(10, 5)

                };

                StackLayout vStack = new StackLayout 
                {
                    Orientation = StackOrientation.Vertical
                };

                StackLayout line1 = new StackLayout
                {
                    Orientation = StackOrientation.Horizontal
                };

                StackLayout line2 = new StackLayout
                {
                    Orientation = StackOrientation.Horizontal
                };

                line1.Children.Add(new Label
                {
                    Text = c.Symbol + " - ",
                    BindingContext = c.Symbol + " - ",
                    FontSize = 15,
                    TextColor = Color.Black,
                    FontAttributes = FontAttributes.Bold
                });

                line1.Children.Add(new Label
                {
                    Text = c.Name,
                    BindingContext = c.Name,
                    FontSize = 15,
                    TextColor = Color.Black,
                    FontAttributes = FontAttributes.Bold
                });

                line1.Children.Add(new Label
                {
                    Text = "Price: " + c.Quote.Usd.Price.ToString("C4"),
                    BindingContext = c.Quote.Usd.Price.ToString("C4"),
                    FontSize = 15,
                    TextColor = Color.Black,
                    HorizontalOptions = LayoutOptions.EndAndExpand
                });

                line2.Children.Add(new Label
                {
                    Text = "24h Change: " + c.Quote.Usd.PercentChange24H.ToString("0.##") + "%",
                    BindingContext = c.Quote.Usd.PercentChange24H,
                    FontSize = 15,
                    TextColor = Color.Black,
                    HorizontalOptions = LayoutOptions.StartAndExpand
                });

                line2.Children.Add(new Label
                {
                    Text = "7d Change: " + c.Quote.Usd.PercentChange7D.ToString("0.##") + "%",
                    BindingContext = c.Quote.Usd.PercentChange7D,
                    FontSize = 15,
                    TextColor = Color.Black,
                    HorizontalOptions = LayoutOptions.EndAndExpand
                });

                vStack.Children.Add(line1);
                vStack.Children.Add(new BoxView {Color = Color.Black, HeightRequest = 2 });
                vStack.Children.Add(new Label
                {
                    Text = "Trade Volume: " + c.Quote.Usd.Volume24H.ToString("C2"),
                    BindingContext = c.Quote.Usd.Volume24H,
                    FontSize = 15,
                    TextColor = Color.Black,
                    HorizontalOptions = LayoutOptions.StartAndExpand
                });
                vStack.Children.Add(new Label
                {
                    Text = "Supply: " + c.TotalSupply.ToString("C2"),
                    BindingContext = c.TotalSupply,
                    FontSize = 15,
                    TextColor = Color.Black,
                    HorizontalOptions = LayoutOptions.StartAndExpand
                });
                vStack.Children.Add(line2);


                f.Content = vStack;

#pragma warning disable CS0618 // Type or member is obsolete
                f.GestureRecognizers.Add(new TapGestureRecognizer((obj) => {
                    CurrencyDetailPage currencyDetailPage = new CurrencyDetailPage(c);//stack.Children[0].BindingContext);
                    this.Navigation.PushAsync(currencyDetailPage);
                }));
#pragma warning restore CS0618 // Type or member is obsolete

                Device.BeginInvokeOnMainThread(() => {
                    CurrenciesStack.Children.Add(f);
                });

            }

            currentState = cmcResponse.Data;

        }

        public void OverviewSearchBar_OnTextChanged(object sender, EventArgs e)
        {

            CurrenciesStack.Children.Clear();

            string keyword = OverviewSearchBar.Text;
            if (keyword == "" || keyword == null)
            {
                var matchingResults = cmcResponse.Data;

                List<Datum> SortedMatches = SortMatches(matchingResults.ToList(), SortOrderPicker.SelectedIndex);
                PopulateOverviewPage(SortedMatches);
                currentState = matchingResults;
            }

            else
            {
                var matchingResults = cmcResponse.Data.Where(c => c.Name.ToLower().Contains(keyword.ToLower()));

                List<Datum> SortedMatches = SortMatches(matchingResults.ToList(), SortOrderPicker.SelectedIndex);
                PopulateOverviewPage(SortedMatches);
                currentState = matchingResults.ToList();
            }
            

        }

        public List<Datum> SortMatches(List<Datum> matchingResults, int selectedOrderIndex)
        {

            if(selectedOrderIndex == 0 || selectedOrderIndex == -1) {

                switch (SortPicker.SelectedIndex)
                {

                    case 0:
                        return matchingResults.OrderBy(c => c.CmcRank).ToList();
                    case 1:
                        return matchingResults.OrderBy(c => c.Name).ToList();
                    case 2:
                        return matchingResults.OrderBy(c => c.Quote.Usd.MarketCap).ToList();
                    case 3:
                        return matchingResults.OrderBy(c => c.Quote.Usd.Price).ToList();
                    case 4:
                        return matchingResults.OrderBy(c => c.Quote.Usd.Volume24H).ToList();
                    case 5:
                        return matchingResults.OrderBy(c => c.CirculatingSupply).ToList();
                    case 6:
                        return matchingResults.OrderBy(c => c.Quote.Usd.PercentChange24H).ToList();
                    default:
                        return matchingResults;
                }

            }
            else
            {
                switch (SortPicker.SelectedIndex)
                {

                    case 0:
                        return matchingResults.OrderByDescending(c => c.CmcRank).ToList();
                    case 1:
                        return matchingResults.OrderByDescending(c => c.Name).ToList();
                    case 2:
                        return matchingResults.OrderByDescending(c => c.Quote.Usd.MarketCap).ToList();
                    case 3:
                        return matchingResults.OrderByDescending(c => c.Quote.Usd.Price).ToList();
                    case 4:
                        return matchingResults.OrderByDescending(c => c.Quote.Usd.Volume24H).ToList();
                    case 5:
                        return matchingResults.OrderByDescending(c => c.CirculatingSupply).ToList();
                    case 6:
                        return matchingResults.OrderByDescending(c => c.Quote.Usd.PercentChange24H).ToList();
                    default:
                        return matchingResults;
                }
            }






            /*if (SortPicker.Items[SortPicker.SelectedIndex] == "Rank")
            {
                var orderResults = currentState.OrderBy(c => c.CmcRank);

                PopulateOverviewPage(orderResults.ToList());
                currentState = orderResults.ToList();
            }

            else if (SortPicker.Items[SortPicker.SelectedIndex] == "Alphabetical")
            {
                var orderResults = currentState.OrderBy(c => c.Name);

                PopulateOverviewPage(orderResults.ToList());
                currentState = orderResults.ToList();
            }

            else if (SortPicker.Items[SortPicker.SelectedIndex] == null)
            {
                var orderResults = currentState;

                PopulateOverviewPage(orderResults);
                currentState = orderResults.ToList();
            }*/

        }

        void CurrencyFrameTapped()
        {

        }

        public async Task GetAllCurrencies()
        {
            //create the httpClient
            HttpClient client = new HttpClient();

            //holds our api key for Coin Market Cap
            string apiKey = "fea5c05e-792e-4675-8acd-6ca3f711337b";

            //Get latest data for all coins from the api
            var response = await client.GetStringAsync("https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?CMC_PRO_API_KEY=" + apiKey);
            //Debug.WriteLine(response);

            apiResponse = response;
        }

        //async void APITest(object sender, System.EventArgs e)
        //{
        //    //create the httpClient
        //    HttpClient client = new HttpClient();

        //    //holds our api key for Coin Market Cap
        //    string apiKey = "fea5c05e-792e-4675-8acd-6ca3f711337b";

        //    //Get latest data for all coins from the api
        //    var response = await client.GetStringAsync("https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?CMC_PRO_API_KEY="+apiKey);
        //    //Debug.WriteLine(response);

        //    var cmcResponse = CoinMarketCapApi.FromJson(response);
        //    //Debug.WriteLine(apiResponse);
        //    apiButton.Text = cmcResponse.Data[0].Name;

        //}
    }
}
