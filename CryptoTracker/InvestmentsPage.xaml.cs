﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace CryptoTracker
{
    public partial class InvestmentsPage : ContentPage
    {

        List<InvestmentInfo> investments;
        List<Datum> currentData;


        public InvestmentsPage()
        {
            InitializeComponent();
            investments = App.DataStorage.GetInvestments();
            currentData = App.APILoader.cmcResponse.Data;
            PopulateInvestmentsPage(investments);

        }


        public void PopulateInvestmentsPage(List<InvestmentInfo> investments)
        {

            investmentsScroller.Children.Clear();

            foreach (InvestmentInfo i in investments)
            {

                Frame f = new Frame
                {
                    BackgroundColor = Color.White,
                    BorderColor = Color.White,
                    CornerRadius = 10,
                    Padding = 10,
                    Margin = new Thickness(10, 5)

                };

                StackLayout vStack = new StackLayout
                {
                    Orientation = StackOrientation.Vertical
                };

                StackLayout line1 = new StackLayout
                {
                    Orientation = StackOrientation.Horizontal
                };

                StackLayout line2 = new StackLayout
                {
                    Orientation = StackOrientation.Horizontal
                };

                line1.Children.Add(new Label
                {
                    Text = i.Name,
                    BindingContext = i.Name,
                    FontSize = 15,
                    TextColor = Color.Black,
                    FontAttributes = FontAttributes.Bold
                });

                line1.Children.Add(new Label
                {
                    Text = "Qty: " + i.AmtInvested.ToString(),
                    BindingContext = i.AmtInvested.ToString(),
                    FontSize = 15,
                    TextColor = Color.Black,
                    HorizontalOptions = LayoutOptions.EndAndExpand
                });



                vStack.Children.Add(line1);
                vStack.Children.Add(new BoxView { Color = Color.Black, HeightRequest = 2 });
                vStack.Children.Add(new Label {
                    Text = "Price when purchased: " + (i.Price * i.AmtInvested).ToString("C4"),
                    BindingContext = (i.Price * i.AmtInvested).ToString("C2"),
                    FontSize = 15,
                    TextColor = Color.Black,
                });
                vStack.Children.Add(new Label
                {
                    Text = "Price now: " + (GetCurrentPrice(i.Name) * i.AmtInvested).ToString("C4"),
                    BindingContext = (GetCurrentPrice(i.Name) * i.AmtInvested).ToString("C4"),
                    FontSize = 15,
                    TextColor = Color.Black,
                });
                vStack.Children.Add(new Label { Text = "--------------------------------"});
                vStack.Children.Add(new Label
                {
                    Text = "Percent change: " + GetPercentChange(i.Price,GetCurrentPrice(i.Name)),
                    BindingContext = i.Price,
                    FontSize = 15,
                    TextColor = Color.Black,
                });

                f.Content = vStack;

#pragma warning disable CS0618 // Type or member is obsolete
                f.GestureRecognizers.Add(new TapGestureRecognizer((obj) =>
                {
                    InvestmentEditPage investmentEditPage = new InvestmentEditPage(i.Name, GetCurrentPrice(i.Name), 1);
                    investmentEditPage.oldPrice = i.Price;
                    investmentEditPage.amtInvested = i.AmtInvested;
                    this.Navigation.PushAsync(investmentEditPage);
                }));
#pragma warning restore CS0618 // Type or member is obsolete

                Device.BeginInvokeOnMainThread(() =>
                {
                    investmentsScroller.Children.Add(f);
                });

            }

        }

        public decimal GetCurrentPrice(string currencyName)
        {

            Datum coin = currentData.First(c => c.Name.ToLower() == currencyName.ToLower());

            return (decimal)coin.Quote.Usd.Price;
        }

        public string GetPercentChange(decimal originalPrice, decimal newPrice)
        {
            return ((newPrice - originalPrice)/originalPrice).ToString("P4");
        }



    }
}
