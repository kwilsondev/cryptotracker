﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace CryptoTracker
{
    public class DataStorage
    {

        string favoritesJSON;
        string investmentsJSON;
        List<Datum> Favorites;
        List<InvestmentInfo> investmentsList = new List<InvestmentInfo>();
        public DataStorage()
        {
            //CLEARInvestments();
            GetFavorites();

            //TODO: initialize the investments list
        }

        public async void AddToFavorites(Datum favoriteIn)
        {
            Favorites.Add(favoriteIn);
            favoritesJSON = JsonConvert.SerializeObject(Favorites);
            App.Current.Properties["Favorites"] = favoritesJSON;
            await Application.Current.SavePropertiesAsync();
        }

        public List<Datum> GetFavorites()
        { 

            //initialize the favorites lists
            if (!App.Current.Properties.ContainsKey("Favorites"))
            {
                favoritesJSON = "[]";
                Favorites = new List<Datum>();

                App.Current.Properties.Add("Favorites", favoritesJSON);

            }
            else
            {
                
                App.Current.Properties.TryGetValue("Favorites", out object favoritesObject);
                favoritesJSON = favoritesObject.ToString();
                Favorites = JsonConvert.DeserializeObject<List<Datum>>(favoritesJSON);
            }

            return Favorites;
        }

        public async void RemoveFavorite(Datum favoriteToRemove){
        
            List<Datum> Favorites = GetFavorites();

            Favorites.Remove(Favorites.First(c => c.Name.ToLower() == favoriteToRemove.Name.ToLower()));

            favoritesJSON = JsonConvert.SerializeObject(Favorites);
            App.Current.Properties["Favorites"] = favoritesJSON;
            await Application.Current.SavePropertiesAsync();


        }

        public async void CLEARFAVORITES()
        {
            App.Current.Properties["Favorites"] = "[]";
            await Application.Current.SavePropertiesAsync();
        }


        //************************************


        public async void AddToInvestments(InvestmentInfo investmentIn)
        {
            investmentsList.Add(investmentIn);
            investmentsJSON = JsonConvert.SerializeObject(investmentsList);
            App.Current.Properties["Investments"] = investmentsJSON;
            await Application.Current.SavePropertiesAsync();
        }

        public List<InvestmentInfo> GetInvestments()
        {
            //initialize the favorites lists
            if (!App.Current.Properties.ContainsKey("Investments"))
            {
                investmentsJSON = "[]";
                investmentsList = new List<InvestmentInfo>();
                App.Current.Properties.Add("Investments", investmentsJSON);

            }
            else
            {
                //App.Current.Properties["Investments"] = "[]";
                App.Current.Properties.TryGetValue("Investments", out object investmentObject);
                investmentsJSON = investmentObject.ToString();
                investmentsList = JsonConvert.DeserializeObject<List<InvestmentInfo>>(investmentsJSON);
            }

            return investmentsList;
        }

        public async void RemoveInvestments(string name)
        {

            List<InvestmentInfo> investmentsList = GetInvestments();
            foreach(InvestmentInfo investment in investmentsList)
            {
                if (investment.Name == name)
                {
                    investmentsList.Remove(investment);
                    break;
                }
            }
            

            investmentsJSON = JsonConvert.SerializeObject(investmentsList);
            App.Current.Properties["Investments"] = investmentsJSON;
            await Application.Current.SavePropertiesAsync();


        }
    }
}
