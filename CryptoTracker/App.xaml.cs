﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace CryptoTracker
{
    public partial class App : Application
    {

        public static DataStorage DataStorage;
        public static APILoader APILoader;

        public App()
        {
            InitializeComponent();

            DataStorage = new DataStorage();
            APILoader = new APILoader();

            NavigationPage navPage = new NavigationPage(new OverviewPage());
            navPage.BackgroundColor = Color.FromHex("#727375");
            navPage.BarBackgroundColor = Color.FromHex("#9b1006");
            navPage.BarTextColor = Color.White;

            MainPage = new MasterDetailPage()
            {
                Master = new MasterPage()
                {
                    Title = "Overview",
                    BackgroundColor = Color.FromHex("#9b1006")
                },

                Detail = navPage
            };
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
