﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CryptoTracker
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InvestmentEditPage : ContentPage
    {
        string name;
        int editOrNew;
        Decimal currentPrice = 0;
        public Decimal amtInvested, oldPrice;
        public InvestmentEditPage()
        {
            InitializeComponent();
        }

        public InvestmentEditPage(string name, Decimal price, int eOn)
        {
            this.name = name;
            this.currentPrice = price;
            this.editOrNew = eOn;
            InitializeComponent();
            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (editOrNew == 0) newInvestment();
            else editInvestment();
        }

        void newInvestment()
        {
            btnDelete.Text = "Cancel";

            lblAmtBought.Text += name + " did you buy?";
        }

        void editInvestment()
        {
            btnDelete.Text = "Sell " + name;
            btnSave.Text = "Cancel";
            Decimal gonnaMake = currentPrice * amtInvested;
            lblAmtBought.Text = "You will be selling " + amtInvested + " for a total of " + (gonnaMake).ToString("C4") + ". This is a difference of " +
                ((currentPrice - oldPrice) * amtInvested).ToString("C4") + " than what you purchased them for. This is a difference of " + 
                ((currentPrice-oldPrice)/currentPrice).ToString("P4");
            entAmt.IsVisible = false;
        }

        private void btnDelete_Clicked(object sender, EventArgs e)
        {
            if (editOrNew == 0) this.Navigation.PopAsync();
            else
            {
                App.DataStorage.RemoveInvestments(name);
                this.Navigation.PopAsync();
            }
        }

        private void btnSave_Clicked(object sender, EventArgs e)
        {
            InvestmentInfo newBoi = new InvestmentInfo();
            newBoi.Name = name;
            newBoi.Price = currentPrice;
            newBoi.AmtInvested = Convert.ToDecimal(entAmt.Text);

            if (editOrNew == 0) App.DataStorage.AddToInvestments(newBoi);
            this.Navigation.PopAsync();
        }

    }
}