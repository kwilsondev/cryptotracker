﻿===========================================================
Name: Tony

What did you work on?
    - Implementing the search bar on the overview page

What did you plan on working on next?
    - Further refining the search and fixing a bug with empty search
    - Adding sort functionality to the oveview page

Obstacles?
    - None thus far
===========================================================
===========================================================
Name: Rob

What did you work on?
    - Added the currencydetail page and made it functional
    - linked the detail page to the overview page fields

What did you plan on working on next?
    - Will work on making the investment object

Obstacles?
    - None thus far
===========================================================
===========================================================
Name: Jay

What did you work on?
    - Further research on APIs
    - Started the class to handle all API calls

What did you plan on working on next?
    - Continuing the API class to handle all calls

Obstacles?
    - None thus far
===========================================================
===========================================================
Name: Ramsey

What did you work on?
    - Looked into local storage

What did you plan on working on next?
    - Looking into the best way the implement the investment UI

Obstacles?
    - Lack of experience in this type of project, Kevin can help if needed
===========================================================
===========================================================
Name: Kevin

What did you work on?
    - Persistent storage
    - Helped get the search functionality implemented
    
What did you plan on working on next?
    - Making a data storage class that will handle all access to persistent storage

Obstacles?
    -None at the moment
===========================================================
===========================================================
Name: Brady

What did you work on?
    -

What did you plan on working on next?
    -

Obstacles?
    -
===========================================================