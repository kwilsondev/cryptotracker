﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoTracker
{
    public class InvestmentInfo
    {
        public InvestmentInfo()
        {

        }

       

        private String name;
        private Decimal price;
        private Decimal amtInvested;

        public string Name { get => name; set => name = value; }
        public decimal Price { get => price; set => price = value; }
        public decimal AmtInvested { get => amtInvested; set => amtInvested = value; }

        
    }
}
